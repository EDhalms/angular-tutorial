var app = angular.module('appModule', []);//создали модуль

var users = [{
    name: 'Petya',
    introdaction: 'I am Petya and I am gigolo…',
    isAdule: true,
    isGigolo: true
},  {
    name: 'Vanya',
    introdaction: 'I am Vanya and I am good boy!',
    isAdule: true,
    isGigolo: false
}, {
    name: 'Lena',
    introdaction: 'I am Lena and I am good girl!',
    isAdule: true,
    isGigolo: false
},  {
    name: 'Sveta',
    introdaction: 'I am Sveta and I am 7 year old)))',
    isAdule: false,
    isGigolo: false
}];

var newEarPersons = [{
        label: 'Дед Мороз (подвыпивший)',
        info: 'Персона уставшая, выпившая за НГ на 3-4х корпоративах. Основная функция - составить компанию взрослым за праздничным столом.',
        cost: 50.45,
        img: 'http://detkamonline.ru/media/icons/1/santas-snowball-showdown.jpg',
        dateStart: 1447290000000,
        dateEnd: 1421614800000
    },
    {
        label: 'Дед Мороз (трезвый)',
        info: 'Работает утром, сдержанный и добрый, предназначен для детей. Постоянно ходит со своей внучкой Снегуркой.',
        cost: 150.99,
        img: 'http://online-raskraski.ru/media/icons/color/1/Snegurka-i-Ded-Moroz_small.jpg',
        dateStart: 1450764000000,
        dateEnd: 1425794400000
    },
    {
        label: 'Санта Клаус',
        info: 'Предназначен для разбалованных детей. Прилагается опция англоговорения. Требование к заказчику – наличие сарая или гаража для парковки оленей!',
        cost: 200,
        img: 'http://online-raskraski.ru/media/icons/color/1/ded-moroz-i-nord-olen_small.jpg',
        dateStart: 1450789200000,
        dateEnd: 1452862800000
    }];


var travel = {
    type: [
        {
            id: 1,
            title: 'Еду с женой'
        }, {
            id: 2,
            title: 'Еду один'
        }
    ],
    events: [
        {
            id: 22,
            typeId: 1,
            title: 'поход в театр'
        }, {
            id: 23,
            typeId: 1,
            title: 'осмотр достопримечательностей'
        }, {
            id: 24,
            typeId: 1,
            title: 'прогулка на катамаране'
        }, {
            id: 25,
            typeId: 2,
            title: 'поход в бар на пляже'
        }, {
            id: 26,
            typeId: 2,
            title: 'дегустация коньяка и виски'
        }, {
            id: 27,
            typeId: 2,
            title: 'поход в шашлычную с пивом'
        }, {
            id: 28,
            typeId: 2,
            title: 'рыбалка'
        }
    ],
    bars: [
        {
            id: 10,
            title: 'Голубая устрица'
        }, {
            id: 11,
            title: 'Розовый кальмар'
        }, {
            id: 12,
            title: 'У Васи'
        }
    ]
};

//lesson 1 - home work
app.controller('firstCtrl', function($scope){
    $scope.friends = users;
});


//lesson 2
app.controller('tabsCtrl', function($scope){
    $scope.newYear = newEarPersons;
    $scope.numbOfTab = 1;
    $scope.setNumbOfTabs = function(numb) {
        $scope.numbOfTab = numb;
    };
    $scope.showTabs = function(numb) {
       return $scope.numbOfTab == numb;
    }
});



//lesson 2 - home task
app.controller('formCtrl', function($scope){
    $scope.turOption = travel;
    $scope.requireValue = true;
    $scope.patternOfName = /^[а-яА-ЯёЁa-zA-Z]+$/;


    $scope.result = {
        name: 'ffff',
        fio: '',
        phone: '',
        comment: '',
        type: $scope.turOption.type[0].id,
        events: [],
        bars: $scope.turOption.bars[0]
    };

    $scope.numb = 1;
    $scope.typeChange = function(numb) {
        $scope.result.type = numb;

    };
    $scope.comparisonType = function(numb) {
        return $scope.result.type == numb;
        //$scope.result.events = [];
    };

});

//lesson 3 and 4 - home task

app.controller('crudCtrl', function($scope, globalCrud){

    $scope.guests  = [
        {id: 11, name: 'Гомер Симпсон', tableNumber: 1, isAdult: true},
        {id: 12, name: 'Мардж Симпсон', tableNumber: 1, isAdult: true},
        {id: 13, name: 'Нед Фландерс', tableNumber: 1, isAdult: true},
        {id: 14, name: 'Апу Нахасапимапетилон', tableNumber: 2, isAdult: true},
        {id: 15, name: 'Манджула Нахасапимапетилон', tableNumber: 2, isAdult: true},
        {id: 16, name: 'Кирк Ван Хутен', tableNumber: 2, isAdult: true},
        {id: 17, name: 'Тим Лавджой', tableNumber: 2, isAdult: true},
        {id: 18, name: 'Вэйлон Смитерс', tableNumber: 3, isAdult: true},
        {id: 19, name: 'Чарльз Монтгомери Бэрнс', tableNumber: 3, isAdult: true},
        {id: 20, name: 'Мо Сизлак', tableNumber: 3, isAdult: true},
        {id: 21, name: 'Милхаус Ван Хутен', tableNumber: 4, isAdult: false},
        {id: 22, name: 'Ральф Виггам', tableNumber: 4, isAdult: false},
        {id: 23, name: 'Эдна Крабаппл', tableNumber: 4, isAdult: true}
    ];

    $scope.requiredField = true;//обязательность заполнения поля
    $scope.smartTable = globalCrud.crud;

    var thisGuestId,
        isEdit = false;

    $scope.newGuest = {};//создаём объект, в котором будем хранить свойства новых/обновлённых гостей

    $scope.renderIsAdult = function(data) {
        if(data == true) {
            return '+';
        } else {
            return '-';
        }
    };

    $scope.deleteGuest = function(guest) {
        var index = $scope.guests.indexOf(guest);

        $scope.smartTable.delete(index, $scope.guests);
    };
    $scope.editGuest = function(guest) {
        isEdit = true;
        var index = $scope.guests.indexOf(guest);//получаем позицию нашего обекта/гостя в массиве/списке гостей
        var thisGuest = $scope.guests[index];// получаем текущий[го] объект/гостя
        thisGuestId = thisGuest.id;//его id

        $scope.newGuest = $scope.smartTable.edit($scope.newGuest, thisGuest);// записывем нового/обновлённого гостя в новый объект, который является моделью для инпутов
    };
    $scope.addNewGuest = function() {
        if(!isEdit) {// гость не редактировался
            $scope.smartTable.add($scope.guests, $scope.newGuest);
            $scope.newGuest = {};//обнуляем наш объект, чтобы очистить поля ввода после сабмита
        }
        else {// гость редактировался
            $scope.guests.forEach(function(item, i){//перебераем массив с гостями
                if(item.id == thisGuestId) { //если id текущего гостя совпадает с id последнего редактируемого гостя, то записываем в значения его свойств значения св-в наших полей для редактирования
                    var editableGuest = $scope.guests[i];
                    editableGuest = $scope.smartTable.edit(editableGuest, $scope.newGuest);
                    $scope.newGuest = {};//обнуляем наш объект, чтобы очистить поля ввода после сабмита
                }
            });
        }
        isEdit = false;
    };

});

app.controller('crudUserCtrl', function($scope, globalCrud){
    $scope.usersNew  = [
        {id: 11, name: 'Тимати', number: 1}
    ];
    $scope.newUser = {};
    $scope.requiredField = true;
    $scope.smartTable = globalCrud.crud;

    var thisUserId,
        isEdit = false;

    $scope.deleteUser = function(user) {
        var index = $scope.usersNew.indexOf(user);
        $scope.smartTable.delete(index, $scope.usersNew);
    };

    $scope.editUser = function(user) {
        isEdit = true;
        var index = $scope.usersNew.indexOf(user);
        var thisUser = $scope.usersNew[index];
        thisUserId = thisUser.id;
        $scope.newUser = $scope.smartTable.edit($scope.newUser, thisUser);
    };

    $scope.addNewUser = function() {
        if(!isEdit) {
            $scope.smartTable.add($scope.usersNew, $scope.newUser);
            $scope.newUser = {};
        }
        else {// гость редактировался
            $scope.usersNew.forEach(function(item, i){
                if(item.id == thisUserId) {
                    var editableUser = $scope.usersNew[i];
                    editableUser = $scope.smartTable.edit(editableUser, $scope.newUser);
                    $scope.newUser = {};
                }
            });
        }
        isEdit = false;
    };

});

